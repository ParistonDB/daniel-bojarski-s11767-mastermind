Projekt o nazwie "Mastermind". Regulamin gry został umieszczony na stronie tego projektu, którą można uruchomić pobierając go i uruchamiając dwie z poniższych komend.

1. npm install
2. bower install

Wymagana baza **MongoDB**

![masta1.png](https://bitbucket.org/repo/LEbAMe/images/2390823646-masta1.png)

![masta2.png](https://bitbucket.org/repo/LEbAMe/images/1128684591-masta2.png)

![masta3.png](https://bitbucket.org/repo/LEbAMe/images/3151002909-masta3.png)