var express                 = require('express'),
    router                  = express.Router(),
    session                 = require('express-session'),
    cookieParser            = require('cookie-parser'),
    mongoose                = require('mongoose'),
    MongoStore              = require('connect-mongo')(session),
    _                       = require('underscore');

//Connecting with the database
mongoose.connect('mongodb://localhost/samurai');

//Checking database connection status
mongoose.connection.on('error', console.error.bind(console, 'connection error:'));
mongoose.connection.once('open', function (callback) {
  console.log("Connected with database.");
});

router.use(cookieParser());

//defining the session informations and store
router.use(session({
    store: new MongoStore({
       mongooseConnection: mongoose.connection
     }),
    secret: 'test session',
    resave: true,
    saveUninitialized: true
}));

//defining main Mastermind game object containing all required and informational fields
var game = {
  codeSize: 0,      //the length of generated secret key
  maxColors: 0,     //the range of numbers of the secret key
  maxMoves: 0,      //max amoumnt of moves for user to guess what's the secret key is
  secret: [],       //a secret key which should be guessed by user (generated randomly using above values)
  inserted: [],     //a table of inserted by users values
  fails: 0,         //an amount of bad quess moves, when fails === codeSize, then user are losing the game
  points: 0         //an amount of guessed points, when points === codeSize, then user are winning the game

  //whole this object is saving as a single session for every user
};

router.use(function(req, res, next) {
    console.log(req.session);
    next();
});

//homepage
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Mastermind' });
});

//a layer where all typed by user information are sending to the session
router.post('/newgame', function(req, res, next) {

  /*
    These settings are for these situations when user clicks the "Start the new game" button.
    In situation like that we have to set all these fields on default values: [], 0, false/true etc.
  */

  game.secret = [];
  game.fails = 0;
  game.points = 0;
  game.inserted = [];

  /*
    Checking if these fields exists in game object - a simple security process.
    If they exists, the only one we need to do is to set the game's fields as the values typed by user.
  */

  if(game.hasOwnProperty("codeSize")) {
    game.codeSize = parseInt(req.body.codeSize, 10);
  }

  if(game.hasOwnProperty("maxColors")) {
    game.maxColors = parseInt(req.body.maxColors, 10);
  }

  if(game.hasOwnProperty("maxMoves")) {
    game.maxMoves = parseInt(req.body.maxMoves, 10);
  }

  /*
    Here is the place where the secret key is generated randomly. Of course typed earlier values by users
    has a role to make the secret key more specified. I mean that the length of secret key shouldn't be longer than
    inserted codeSize value, the range of generated number in the secret key should be wider than 0-maxMoves.
  */

  for(var i = 0; i < game.codeSize; i++) {
    game.secret.push(Math.floor((Math.random() * game.maxColors) + 0));
  }

  //That's the place when the game object is assigned to the user's session.
  req.session.game = game;

  console.log(game);
  next();
});

//a single user guess layer (all these values are sending to the session)
router.post('/move', function(req, res, next) {

  /*
    Until user will send his first guess numbers, defaultly the "inserted" table is empty. Firstly we check for
    security if the table exists in the game object. If yes, we are making a for loop of length equality to an
    amount of typed numbers and in this loop we just assaigning the inserted numbers to table. Firstly we need to
    parse all these numbers because defaultly they are sended here as a String objects.
  */

  if(game.hasOwnProperty("inserted")) {
    for(var i = 0; i < req.body.inserted.length; i++) {
      req.session.game.inserted[i] = parseInt(req.body.inserted[i], 10);
    }
  }

  var mark = function (code, move) {

    var calcBlack = function() {
      return _.size(_.filter(code, function(value, index) {
        return value === move[index];
      }));
    };

    var calculateWhites = function () {
      return _.countBy(code, function(num) {
        return num;
      });
    };

    var whiteFinder = function(code, move) {
      var appearancesMap = calculateWhites(code);
      return function(element) {
        if(appearancesMap[element] > 0){
          appearancesMap[element]--;
          return true;
        }
        return false;
      };
    };

    var calcWhite = function() {
      return _.filter(move, whiteFinder(code, move));
    };

    return {
      black: calcBlack(),
      white: calcWhite(code, move).length - calcBlack(code, move)
    };
  };

  /*
    A simple mathematic to calculate properly guessed by user numbers. The "black" object from the "mark"
    function means the same number value with the same index in table.
  */

  req.session.game.points = mark(req.session.game.secret, req.session.game.inserted).black;

  //These ifs are mostly for debugging
  if(req.session.game.points === req.session.game.codeSize) {
    console.log("Wygrales!");
  } else {
    req.session.game.fails++;
    console.log("Pomylka! Pozostale proby: "+(req.session.game.maxMoves-req.session.game.fails));
  }

  if(req.session.game.fails >= req.session.game.maxMoves) {
    console.log("Przegrales!");
  }

  console.log(req.session.game.inserted);

  /*
    The last thing to do is to send the session which contains all the game objects as a json to jquery file.
    There we can easily manage all these informations and dependently of final result, we can for example
    insert html blocks with specified messages.
  */

  res.json(req.session.game);
});

/*
  When the user wins or loses the game, a good idea is to remove his unnecessary now session. The session will be
  created again when the user will click "Start the game" button.
*/

router.post('/clearSession', function(req, res, next) {
  req.session.destroy();
  next();
});

module.exports = router;
